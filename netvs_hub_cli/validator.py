

class Validator:
    """
    Base class validator to be inherited from.
    """

    def __init__(self):
        """
        Empty constructor
        """
        # Nothing to do here
        pass

    def validate(self, json_instance: dict):
        """
        Empty method, which only checks if the given parameter is not None

        :param json_instance: Python dictionary which can't be None.
        :return: Nothing
        """
        if json_instance is None:
            raise AttributeError("JSON dict can't be empty.")
