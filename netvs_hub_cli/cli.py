import json
import sys
import os.path
import typing

import click

import jsonschema

from netvs_hub_cli.type_validator import TypeValidator
from netvs_hub_cli.validator import Validator

from netvs_hub_cli import CURRENT_SCHEMA_VERSION
from netvs_hub_cli.base_validator import BaseValidator
from netvs_hub_cli.exception import NetvsHubException
from netvs_hub_cli.netapi_validator import NetApiValidator


@click.command("netvs-hub-cli", help="""\
Checks JSON files against NETVS-Hub Schemas.
""")
@click.help_option("-h", "--help")
@click.option("--endpoint", type=click.Choice(['Devel', 'Prod'], case_sensitive=False),
              default=lambda: os.environ.get("NETDB_ENDPOINT", "Prod"),
              help="Endpoint to use. This will use the NETDB_ENDPOINT environment value as default if available. This"
                   "parameter is case-insensitive.")
@click.option("--export-index", is_flag=True, show_default=True, default=False,
              help="Export an index.json file with an summary of all valid JSON templates.")
@click.version_option()
@click.option(
    "--schemas",
    help=(
        "The path to the folder containing the NETVS-Hub JSON Schemas to use."
    ),
    required=True,
    metavar='<schemas>'
)
@click.argument("instance_files", required=True, nargs=-1)
def main(schemas: str, endpoint, export_index, instance_files: tuple[str, ...],) -> None:

    # Define a constant, to check, if any of those files creates errors.
    has_errors = [False] * len(instance_files)
    error_count = [0] * len(instance_files)

    is_prod = True if endpoint == "Prod" else False

    has_duplicates = False

    # Check, if there are possible duplicates
    if len(instance_files) != len(list(set(instance_files))):
        has_duplicates = True
        click.echo(click.style("Warning", fg='yellow', bold=True), nl=False)
        click.echo(click.style(": There may be duplicate entries in the instance files lists.", fg='yellow'))

    # Iterate over all files and check those
    for index, file_path in enumerate(instance_files):

        # Try to iterate over all given files
        try:
            instance_file = open(file_path)
        except FileNotFoundError:
            has_errors[index] = True
            click.echo(click.style(f"Couldn't find file {file_path}!", fg='bright_red'))
            continue

        # Convert the schema file to a python dict
        try:
            json_instance = json.load(instance_file)
        except json.JSONDecodeError:
            has_errors[index] = True
            click.echo(click.style("Couldn't load file as dict!", fg='bright_red'))
            continue

        # Load the schema version from the user file
        if "schema_version" in json_instance:
            if isinstance(json_instance['schema_version'], int):
                json_schema_version = json_instance['schema_version']
            else:
                has_errors[index] = True
                click.echo(click.style("Schema version must be a string!", fg='bright_red'))
                continue
        else:
            has_errors[index] = True
            click.echo(click.style("Couldn't find schema version in the given file!", fg='bright_red'))
            continue

        # Throw an error, if the user supplied version is smaller than the current supported version
        if json_schema_version != CURRENT_SCHEMA_VERSION:
            has_errors[index] = True
            error_count[index] += 1
            click.echo(click.style(f"The specified API Version {json_schema_version} is not the currently "
                                   f"supported version {CURRENT_SCHEMA_VERSION}.", fg='bright_red'))

        # Load the schema from the path and the version
        try:
            schema = load_schema(schemas, json_schema_version)
        except NetvsHubException as ex:
            has_errors[index] = True
            click.echo(click.style(ex.message, fg='bright_red'))
            continue

        # Validate against the NETVS-Hub schema

        # Create a validator instance for the latest draft
        json_schema_validator = jsonschema.validators.Draft202012Validator(schema)

        # Get a list of errors
        json_schema_errors = sorted(json_schema_validator.iter_errors(json_instance), key=lambda e: e.path)

        # Update the error status, if there are errors
        if len(json_schema_errors) > 0:
            has_errors[index] = True
            # Increase the global error count by the amount of current errors
            error_count[index] += len(json_schema_errors)

        # Iterate over the errors
        for error in json_schema_errors:
            click.echo(f"{error.message} for {file_path}")

        # Define all validators
        validators = [BaseValidator(), NetApiValidator(is_prod), TypeValidator()]
        # Validate the schema against all the defined validators
        returned_errors, count = validate(file_path=file_path, json_instance=json_instance, validators=validators)

        # Update the error status, if there are errors
        if returned_errors:
            has_errors[index] = True
        if count > 0:
            has_errors[index] = True
            # Increase the global error count by the amount of current errors
            error_count[index] += count

        # Do clean up
        instance_file.close()

    # Print result
    click.echo(click.style("= Summary =", bold=True))
    # Iterate over all files and print the result
    for index, file_path in enumerate(instance_files):
        if has_errors[index]:
            if error_count[index] == 0:
                click.echo(f"Internal errors for {file_path}")
            else:
                click.echo(f"{error_count[index]} errors for {file_path}")

    # If there's any error quit with a non-zero status code
    if any(has_errors):
        click.echo("At least one error was found, therefore exiting with -1.")
        # Exit with an error code
        sys.exit(-1)
    else:
        # All files were successfully validated with no errors at all. Print message for user feedback
        click.echo("The validation for all files was completed successfully without errors.")

        if not has_duplicates and export_index:
            # Do the export
            export_index_file(has_errors=has_errors, error_count=error_count, instance_files=instance_files)


def validate(file_path: str, json_instance: dict, validators: typing.List[Validator]) -> typing.Tuple[bool, int]:
    has_errors = False
    error_count = 0

    for validator in validators:
        errors = validator.validate(json_instance=json_instance)

        # Update the error status, if there are errors
        if len(errors) > 0:
            has_errors = True
            # Increase the global error count by the amount of current errors
            error_count += len(errors)

        # Iterate over the errors
        for error in errors:
            click.echo(f"{error.message} for {file_path}")

    return has_errors, error_count


def load_schema(path: str, json_schema_version: int) -> dict:
    # Load the schema file
    # Normalize the given path
    full_path = f"{os.path.normpath(path)}{os.sep}version_{json_schema_version}.schema.json"
    try:
        file = open(full_path)
    except FileNotFoundError:
        raise NetvsHubException(f"Couldn't find file {full_path}!")

    # Convert the schema file to a python dict
    try:
        schema = json.load(file)
    except json.JSONDecodeError:
        raise NetvsHubException("Couldn't load file as dict!")

    # Check, if the schema is valid
    try:
        jsonschema.validators.Draft202012Validator.check_schema(schema)
    except jsonschema.exceptions.SchemaError:
        raise NetvsHubException("Invalid NETVS-Hub JSON schema!")
    # Do clean up
    file.close()
    # Return the schema
    return schema


def export_index_file(has_errors, error_count, instance_files: tuple[str, ...]):
    # Try generating an index
    index_list = []
    for index, file_path in enumerate(instance_files):
        if not has_errors[index] and error_count[index] == 0:
            # Add the template to the index file

            # Load the file
            try:
                file = open(file_path)
                json_dict = json.loads(file.read())

            except FileNotFoundError:
                continue

            index_list.append({"file_name": os.path.basename(file_path),
                               "schema_version": json_dict["schema_version"],
                               "author": json_dict["author"],
                               "target_api": json_dict["target_api"],
                               "name": json_dict["name"],
                               "description": json_dict["description"]})
            # Close the file
            file.close()
    # Try writing the file to disk
    try:
        # Try opening the file and ensure it's utf-8 encoded
        output_file = open("index.json", "w", encoding="utf-8")
        json.dump(index_list, output_file)
        output_file.close()
    except OSError:
        click.echo(click.style("Couldn't write index.json file!", fg='bright_red'))
