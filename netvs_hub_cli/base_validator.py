import re
import typing

from netvs_hub_cli.exception import NetvsHubError
from netvs_hub_cli.validator import Validator
from netvs_hub_cli import SUPPORTED_TARGET_API_VERSION


class BaseValidator(Validator):

    def __init__(self):
        # Call the parent constructor
        super().__init__()

    def validate(self, json_instance: dict) -> typing.List:
        # Call the parent method
        super().validate(json_instance)

        # Create a list of errors
        errors = []

        # Do validation stuff

        # Check if the key exists
        if "target_api" in json_instance:
            if json_instance["target_api"] != SUPPORTED_TARGET_API_VERSION:
                errors.append(NetvsHubError("'target_api' is not the currently supported api version."))

        # Check if all transaction items have a unique idx
        if "transaction" in json_instance:
            transactions = json_instance["transaction"]

            idx_list = []

            # Check if every item has an idx property
            for item in transactions:
                # Throw an error if the property is missing
                if "idx" not in item:
                    errors.append(NetvsHubError("Missing idx-field for transaction item"))
                else:
                    # If the property is defined well, check if it's already part of the list
                    if item["idx"] not in idx_list:
                        idx_list.append(item["idx"])
                    # Otherwise throw an error due to duplication
                    else:
                        errors.append(NetvsHubError(f"{item['idx']} key is defined more than once!"))

        # Check if all variables are defined
        if "transaction" in json_instance and "variables" in json_instance:
            # Extract all strings
            all_strings = self._extract_all_strings(json_instance["transaction"])

            # Compile regex
            regex = re.compile("{{\\s*(\\S+)\\s*}}")

            # Check if all called string
            for string in all_strings:
                # Iterate over all matches in the string
                for match in re.finditer(regex, string):
                    # Check if the matched parameter is in all parameters or throw an error
                    if match.group(1) not in json_instance["variables"]:
                        errors.append(NetvsHubError(f"Variable {match.group(1)} is not defined"))

        # Return the result
        return errors

    def _extract_all_strings(self, json_instance=None) -> typing.List:
        result = []
        if json_instance is None:
            return result
        if isinstance(json_instance, list):
            for item in json_instance:
                # Append the item
                result += self._extract_all_strings(item)
        if isinstance(json_instance, dict):
            for key, value in json_instance.items():
                if isinstance(value, str):
                    result.append(value)
                if isinstance(value, dict) or isinstance(value, list):
                    result += self._extract_all_strings(value)
        # Return the list with all found strings inside it
        return result
