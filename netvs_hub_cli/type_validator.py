import datetime
import dns.name
import ipaddress
import json
import re
import typing

import click

from netvs_hub_cli.exception import NetvsHubError

from netvs_hub_cli.validator import Validator


class TypeValidator(Validator):

    def __init__(self):
        # Call the parent constructor
        super().__init__()

    def validate(self, json_instance: dict) -> typing.List:
        # Call the parent method
        super().validate(json_instance)

        # Create an error list
        errors = []

        # Do validation stuff
        if "variables" in json_instance:
            # Get all variables
            for item in json_instance["variables"]:
                if "type" in item and "default" in item and "id" in item:
                    # Check if the default has the correct type
                    if item["type"] == "str" or item["type"] == "mac_addr":
                        # Continue on strings as there is nothing to check against
                        # Do similar with Mac addresses as they have a wild format
                        continue
                    elif item["type"] == "bool":
                        if not isinstance(json.loads(item["default"]), bool):
                            # Add an error if the parameter is not a bool
                            errors.append(NetvsHubError(f"Expected bool for {item['id']}"))
                    elif item["type"] == "int":
                        if not isinstance(json.loads(item["default"]), int):
                            # Add an error if the parameter is not an int
                            errors.append(NetvsHubError(f"Expected integer for {item['id']}"))
                    elif item["type"] == "date":
                        try:
                            # Try parsing the given date
                            datetime.datetime.strptime(item["default"], "%Y-%m-%d")
                        except ValueError:
                            # Add an error to the list
                            errors.append(NetvsHubError(f"Expected date with format YYYY-MM-DD for {item['id']}"))
                    elif item["type"] == "datetime":
                        try:
                            # Try finding a match
                            match = re.match("^\\d{4}-\\d{2}-\\d{2}T\\d{2}24:\\d{2}:\\d{2}.\\d{6}[+-]\\d{2}:\\d{2}$",
                                             item["default"])

                            if match is None:
                                # Add an error to the list
                                errors.append(NetvsHubError(f"{item['id']} isn't matching the postgres timestamp "
                                                            "format"))
                            else:
                                # Try checking the given postgres timestamp
                                # Remove the 24 suffix after the %H and the : in the timezone definition to make it ISO
                                # compliant.
                                datetime.datetime.strptime(item["default"][:13] + item["default"][15:31] +
                                                           item["default"][32:], "%Y-%m-%dT%H:%M:%S.%f%z")
                        except ValueError:
                            # Add an error to the list
                            errors.append(NetvsHubError("Expected datetime with format "
                                                        "YYYY-MM-DDTHH24:mm:ss.ffffff+TZH:TZM"
                                                        f"(Postgres format) for {item['id']}"))
                    elif item["type"] == "ip_addr":
                        try:
                            # Try to parse the given ip address
                            ipaddress.ip_address(item["default"])
                        except ValueError:
                            # Add an error to the list
                            errors.append(NetvsHubError(f"Expected IP address for {item['id']}"))
                    elif item["type"] == "fqdn":
                        try:
                            dns.name.from_text(item["default"].strip().lower())
                        except (ValueError, dns.name.BadEscape, dns.name.EmptyLabel):
                            errors.append(NetvsHubError(f"Expected valid fqdn for {item['id']}"))
                    elif item["type"] == "typeahead":
                        if "type_params" not in item:
                            errors.append(NetvsHubError("Missing required type_params!"))
                        else:
                            if "query" not in item["type_params"]:
                                errors.append(NetvsHubError("Missing required query parameter!"))
                            if "query_path" not in item["type_params"]:
                                errors.append(NetvsHubError("Missing required query_path parameter!"))
                            if "display_value" not in item["type_params"]:
                                errors.append(NetvsHubError("Missing required display_value parameter!"))
                            if "return_value" not in item["type_params"]:
                                errors.append(NetvsHubError("Missing required return_value parameter!"))
                    elif item["type"] == "ip_addr":
                        if "type_params" not in item:
                            errors.append(NetvsHubError("Missing required type_params!"))
                        else:
                            if "address_type" not in item["type_params"]:
                                errors.append(NetvsHubError("Missing required address_type parameter!"))
                            else:
                                if not isinstance(item["type_params"]["address_type"], str):
                                    errors.append(NetvsHubError("Parameter address_type must be a string!"))
                                elif not item["type_params"]["address_type"] in ("v4", "v6", "v4+v6"):
                                    errors.append(NetvsHubError("Unknown value for address_type parameter!"))
                    elif item["type"] == "select":
                        # Check if the requisits for a select are given
                        if "type_params" not in item:
                            errors.append(NetvsHubError("Missing required type_params!"))
                        else:
                            if "entries" not in item["type_params"]:
                                errors.append(NetvsHubError("Missing required entries parameter!"))
                            elif not isinstance(item["type_params"]["entries"], list):
                                errors.append(NetvsHubError("Parameter entries must be a list of objects!"))
                            else:
                                # Check if all entries have the required values
                                for entry in item["type_params"]["entries"]:
                                    if "display_name" not in entry:
                                        errors.append(NetvsHubError("Entry must contain a display name!"))
                                    elif not isinstance(entry["display_name"], str):
                                        errors.append(NetvsHubError("Display name must be a string!"))
                    else:
                        click.echo(f"Skipping unknown type {item['type']}")

        return errors
