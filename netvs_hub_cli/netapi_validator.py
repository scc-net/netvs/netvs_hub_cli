import jsonschema
import os
import requests
import typing

from netvs_hub_cli import SUPPORTED_TARGET_API_VERSION
from netvs_hub_cli.exception import NetvsHubError
from netvs_hub_cli.validator import Validator

from netdb_client.api41 import APIEndpoint, APISession, cntl

_NETAPI_PROD_BASE_URL = "api.netdb.scc.kit.edu"
_NETAPI_DEVEL_BASE_URL = "api.netdb-devel.scc.kit.edu"
_NETAPI_PROD_URL = f"https://{_NETAPI_PROD_BASE_URL}/"
_NETAPI_DEVEL_URL = f"https://{_NETAPI_DEVEL_BASE_URL}/"


class NetApiValidator(Validator):

    def __init__(self, is_prod: bool = True):
        # Call the parent constructor
        super().__init__()

        if "NETDB_TOKEN" not in os.environ:
            raise AttributeError("NETDB_TOKEN not defined!")

        # Set the value
        self.is_prod = is_prod

        url = _NETAPI_PROD_URL if self.is_prod else _NETAPI_DEVEL_URL
        response = requests.get(url)
        # Check if the request was successful
        if response.status_code != 200:
            raise AttributeError("Couldn't access NETAPI!")

        # Load the json
        json_response = response.json()

        has_found = False

        # Try finding a matching NET-API version
        for item in json_response[0]:
            # Check all entries
            if f"{item['major']}.{item['minor']}" == SUPPORTED_TARGET_API_VERSION:
                has_found = True
                self.version = item

        if not has_found:
            raise AttributeError("Couldn't find a supported version in the NET-API!")

        # Create an NETDB endpoint instance
        endpoint = APIEndpoint(base_url=_NETAPI_PROD_BASE_URL if self.is_prod else _NETAPI_DEVEL_BASE_URL,
                               token=os.getenv("NETDB_TOKEN"))
        # Initialize an API session from the previously defined endpoint to access the NETDB with it.
        api = APISession(endpoint)

        api_response = cntl.Role.list(api_session=api, is_role_old=False)

        # Ensure that the NETDB api returns a response
        if not api_response:
            raise ValueError("Couldn't load result!")

        # Extract the permission names from the role list
        self.permissions = [x.fq_name for x in cntl.Role.list(api_session=api, is_role_old=False)]

    def validate(self, json_instance: dict) -> typing.List:
        # Call the parent method
        super().validate(json_instance)

        # Create a list of errors
        errors = []

        # Do validation stuff

        # Create a json schema
        json_schema = self.version["transaction_json_schema"]

        json_schema_validator = jsonschema.validators.Draft202012Validator(json_schema)

        # Check if the provided schema is actually valid, just to be safe
        try:
            jsonschema.validators.Draft202012Validator.check_schema(json_schema)
        except jsonschema.exceptions.SchemaError:
            errors.append(NetvsHubError("Loaded NET-API schema is not valid!"))
            return errors

        # Select only the transaction part of the json_instance

        if "transaction" in json_instance:
            transactions = json_instance["transaction"]

            json_schema_errors = sorted(json_schema_validator.iter_errors(transactions), key=lambda e: e.path)

            for error in json_schema_errors:
                errors.append(NetvsHubError(error.message))

        if "required_permissions" in json_instance:
            permission_list = json_instance["required_permissions"]

            for permission_item in permission_list:
                if permission_item not in self.permissions:
                    errors.append(NetvsHubError(f"Unknown permission: {permission_item}"))

        # Return the result
        return errors
