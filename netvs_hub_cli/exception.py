
class NetvsHubException(Exception):
    def __init__(self, message=None):
        self.message = message if message else ""

    def __str__(self):
        return self.message


class NetvsHubError:
    def __init__(self, message=None):
        self.message = message if message else ""

    def __str__(self):
        return self.message
