# netvs_hub_cli

The NETVS-Hub CLI is a command line interface to validate NETVS-Hub JSON-files against the defined schemas and do more
advanced checks.

## Requirements

- Python >=3.6, due to f-strings
- click
- jsonschema
- requests
- Latest version of the [netdb-client](https://gitlab.kit.edu/scc-net/netvs/netdb-client)

To validate successfully with the NETVS-Hub CLI a working internet connection is needed.

## Setup

Installation is done using the following steps:

1. Install the [netdb-client](https://gitlab.kit.edu/scc-net/netvs/netdb-client) according to the provided documentation
2. Configure the `netdb-client` by creating the `~/.config/netdb_client.ini`
3. Executing the following command
    ```shell
    pip3 install git+https://gitlab.kit.edu/scc-net/netvs/netvs_hub_cli.git@main#egg=netvs_hub_cli
    ```

## Usage

Check all files in the current directory to the production endpoint

```shell
python -m netvs_hub_cli --schemas /path/schemas/ *.json
```

Check a single file against the devel endpoint

```shell
python -m netvs_hub_cli --schemas /path/schemas/ --endpoint=devel test.json
```

Get usage help:

```shell
python -m netvs_hub_cli --help
```

Get current version:

```shell
python -m netvs_hub_cli --version
```

## License

The NET-SUITE is licensed under the MIT-License. More information can be found [here](LICENSE).
